
package javaapplication3;

/**
 *
 * @author student
 */

public class JavaApplication3 {

  public static void main(String[] args) {
        System.out.println("Witaj, Świecie!");
        
        przykladowaMetoda();
        int wynikDodawania = dodajLiczby(5, 3);
        System.out.println("Wynik dodawania: " + wynikDodawania);
    }
    
    // Przykładowa metoda bez argumentów
    public static void przykladowaMetoda() {
        System.out.println("To jest przykładowa metoda!");
    }
    
    // Metoda zwracająca sumę dwóch liczb
    public static int dodajLiczby(int a, int b) {
        return a + b;
    }
}
    
    
    

